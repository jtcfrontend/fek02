$(document).ready(function(){
    var postsUrl = "";
    var categoryList = [
        {
            category: "vanhoa",
            url: "https://api.myjson.com/bins/kcocx"
        },
        {
            category: "thethao",
            url: "https://api.myjson.com/bins/kcocxsssssss"
        }
    ]
    var detailCodeParam = getParameterByName("detailCode");
    var categoryParam = getParameterByName("category");
    
    for(var i = 0; i < categoryList.length; i++) {
        if(categoryList[i].category == categoryParam) {
            postsUrl = categoryList[i].url;
        }
    }
    
    $.ajax({
        url: postsUrl,
        method:"GET",
        success:function(posts) {
            for(var i = 0; i < posts.length; i++) {
                if(posts[i].detailCode == detailCodeParam) {
                    $("body").append("<br> cua " + posts[i].title);
                    break;
                }
            }            
        },
        error:function(error){
            console.log(error);
        }
    })
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}